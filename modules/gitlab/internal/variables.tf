variable "gitlab_project_id" {
  type = string
}

variable "gitlab_runner_description" {
  type = string
}

variable "gitlab_runner_tags" {
  type = list(string)
}

variable "name" {
  type = string
}

