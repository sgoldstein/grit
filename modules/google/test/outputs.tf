output "gke_cluster_name" {
  value = module.test-module.gke_cluster_name
}

output "gke_cluster_host" {
  value = module.test-module.gke_cluster_host
}

output "gke_cluster_access_token" {
  value = module.test-module.gke_cluster_access_token
}

output "gke_cluster_ca_certificate" {
  value = module.test-module.gke_cluster_ca_certificate
}

